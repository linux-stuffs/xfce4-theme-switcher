#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Name: xfce-theme-switcher.py
# Short-Description: Xfce4 Theme Switcher GUI for theme-switcher.sh
# Version: 0.1.5

# Provides: xfce4-theme-switcher.py
# Requires: bash, python3, python3-gi

# Developer: Pavel Sibal <entexsoft@gmail.com>
#
# Xfce4 Theme Switcher.py (xfce4-theme-switcher.py) is python3 script 
# which working like GUI for theme-switcher.sh.

# Allow to you by the very easy way change your xfce4 desktop theme, background,
# panels, plank, conky, backup and restore your setting, etc..
#
# Xfce4 Theme Switcher.py (xfce4-theme-switcher.py) is free software: you 
# can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation, either version
# 3 of the License, or (at your option) any later version.
# You can get a copy of the license at www.gnu.org/licenses
#
# Xfce4 Theme Switcher.py (xfce4-theme-switcher.py) is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# <http://www.gnu.org/licenses/>.

# Run:
# xfce4-theme-switcher.py

import subprocess, gi, os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from pathlib import Path


class MainWindow:
    
    filename = None
    
    def __init__(self):
        self.home_dir = str(Path.home())
        fileDir = os.path.dirname(os.path.abspath(__file__))
        self.theme_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'themes')
        self.backup_dir = os.path.join(self.home_dir, '.backup-xts')
        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'xfce4-theme-switcher.glade'))
        self.theme_switcher_script = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'theme-switcher.sh')
        self.builder.connect_signals(self)
        self.window = self.builder.get_object("MainWindow")
        self.tw_themes = self.builder.get_object('tw_themes')
        self.tm_themes = self.tw_themes.get_model()
        self.tw_backups = self.builder.get_object('tw_backups')
        self.tm_backups = self.tw_backups.get_model()
        self.apply_dialog = self.builder.get_object("apply_dialog")
        self.backup_dialog = self.builder.get_object("backup_dialog")
        self.create_skeleton_dialog = self.builder.get_object("create_skeleton_dialog")
        self.select_directory_but1 = self.builder.get_object("select_directory_but1")
        self.pack_skeleton_dialog = self.builder.get_object("pack_skeleton_dialog")
        self.select_directory_but2 = self.builder.get_object("select_directory_but2")
        self.theme_name_entry = self.builder.get_object("theme_name_entry")
        self.backups_load_directory()
        self.themes_load_directory()
        self.window.show()


    def apply_dialog_apply(self, *args):
        if self.filename is not None:
            subprocess.run("%s -tl %s" % (self.theme_switcher_script, self.filename), shell=True)
        self.apply_dialog.hide()


    def apply_dialog_cancel(self, *args):
        self.apply_dialog.hide()


    def backup_dialog_backup(self, *args):
        subprocess.run("%s -b" % (self.theme_switcher_script), shell=True)
        self.backups_load_directory()
        self.backup_dialog.hide()


    def backup_dialog_cancel(self, *args):
        self.backup_dialog.hide()


    def create_skeleton_dialog_cancel(self, *args):
        self.create_skeleton_dialog.hide()


    def create_skeleton_dialog_create(self, *args):
        directory = self.select_directory_but1.get_file()
        if directory is not None:
            subprocess.run("%s -sc %s" % (self.theme_switcher_script, directory.get_path()), shell=True)
            self.create_skeleton_dialog.hide()


    def pack_skeleton_dialog_cancel(self, *args):
        self.pack_skeleton_dialog.hide()


    def pack_skeleton_dialog_pack(self, *args):
        name = self.theme_name_entry.get_text() 
        directory = self.select_directory_but2.get_file()
        if name != "" and directory is not None:
            subprocess.run("%s -sp %s %s" % (self.theme_switcher_script, directory.get_path(), name.replace(" ", "_")), shell=True)
            self.pack_skeleton_dialog.hide()

    
    def apply_changes(self, *args):
        self.apply_dialog.run()
        self.apply_dialog.hide()


    def create_backup(self, *args):
        self.backup_dialog.run()
        self.backup_dialog.hide()


    def create_skeleton(self, *args):
        self.create_skeleton_dialog.run()
        self.create_skeleton_dialog.hide()


    def pack_skeleton(self, *args):
        self.pack_skeleton_dialog.run()
        self.pack_skeleton_dialog.hide()


    def switch_page(self, notebook, page, page_num):
        if page_num == 1:
            self.backups_get_selected(self.tw_backups)
        else:
            self.themes_get_selected(self.tw_themes)


    def themes_doubleclick(self, *args):
        model, row = self.tw_themes.get_selection().get_selected()
        if row is not None:
            self.filename = model[row][1]
            self.apply_dialog.run()
            self.apply_dialog.hide()


    def backups_doubleclick(self, *args):
        model, row = self.tw_backups.get_selection().get_selected()
        if row is not None:
            self.filename = model[row][1]
            self.apply_dialog.run()
            self.apply_dialog.hide()


    def themes_get_selected(self, widget):
        model, row = self.tw_themes.get_selection().get_selected()
        if row is not None:
            self.filename = model[row][1]


    def backups_get_selected(self, widget):
        model, row = self.tw_backups.get_selection().get_selected()
        if row is not None:
            self.filename = model[row][1]


    def themes_load_directory(self):
        self.tm_themes.clear()
        for line in self.get_listdir(self.theme_dir):
            self.tm_themes.append(line)


    def backups_load_directory(self):
        self.tm_backups.clear()
        if os.path.isdir(self.backup_dir) == False:
            os.makedirs(self.backup_dir)
        else:
            for line in self.get_listdir(self.backup_dir, True):
                self.tm_backups.append(line)


    def get_listdir(self, path, rev=False):
        items = []
        for x in sorted(os.listdir(path), key=str.casefold, reverse=rev):
            if x.endswith('.tar.gz'):
                name = x[:-7]
                pth = os.path.join(path, x)
                items.append((name, pth))
        return items

    def on_help(self, *args):
        screen = self.window.get_screen()
        link = "file:///usr/share/doc/xfce4-theme-switcher/help.html"
        Gtk.show_uri(screen, link, Gtk.get_current_event_time())


    def on_close(self, *args):
        Gtk.main_quit()



if __name__ == "__main__":
    main = MainWindow()
    Gtk.main()
