#!/bin/bash
# Name: theme-switcher
# Short-Description: bash script for Xfce4 Theme Switcher
# Version: 0.1.8

# Provides: theme-switcher.sh
# Requires: bash, cat, sed, xfce4, python3 xfconf-query, dconf, tar, notify-send

# Developer: Pavel Sibal <entexsoft@gmail.com>
#
# Theme Switcher (theme-switcher) is comand-line script for configuration of the
# Xfce4 desktop.
# Allow to you by the very easy way change your xfce4 desktop theme, background,
# panels, plank, conky, backup and restore your setting, etc..
#
# Theme Switcher (theme-switcher) is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# You can get a copy of the license at www.gnu.org/licenses
#
# Theme Switcher (theme-switcher) is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# <http://www.gnu.org/licenses/>.

# Run:
# theme-switcher [OPTION]

SHARE_DIR=$(dirname "$(realpath $0)")

WORKING_DIR=~/.cache/xfce4-theme-switcher/
BACKUP_DIR=~/.backup-xts/
XFCE4_PANEL_PROFILES=$SHARE_DIR/desktopconfig.py
PANEL_PROFILE=$WORKING_DIR/panel-profile.tar.gz

mkdir -p ~/.config/xfce4-theme-switcher/
mkdir -p $BACKUP_DIR
mkdir -p $WORKING_DIR

# Hide existing notifications
# killall xfce4-notifyd
ps -C xfce4-notifyd > /dev/null && killall xfce4-notifyd

help()
{
  cat <<-EOF
Usage: xfce4-theme-switcher [OPTION]

  -h,  --help                 show this help

  -b,  --backup               backup actual settings of the Xfce4 theme

  -r,  --restore file file-name
                              restore settings from file (from the ~/.backup-xts)

  -rl, --restore-last	      restore last backup file (from the ~/.backup-xts)

  -l,  --list                 list of all themes and backup files

  -lb, --list-backups         list of backup files only (from the ~/.backup-xts)

  -lt, --list-themes          lits of themes only
                              (/usr/share/xconf-theme-switcher/themes)

  -sc, --skeleton-create /full-path-directory/
                              create skeleton from the actual settings
                              in the full-path directory

  -sp, --skeleton-pack full-path/ theme-name
                              pack the skeleton from the full-path directory
                              like theme-name (the file extension tar.gz
                              is automatically added)

  -t,  --theme-set theme-name set theme from the themes directory
                              (/usr/share/xconf-theme-switcher/themes)

  -tl, --theme-load /full-path-directory/file-name
                              load theme from selected file
EOF
}

set_settings()
{
	 killall xfce4-panel
	[ -f ~/.config/xfce4-theme-switcher/uninstall.sh ] && bash ~/.config/xfce4-theme-switcher/uninstall.sh
	[ -f $WORKING_DIR/uninstall.sh ] && cp -rf $WORKING_DIR/uninstall.sh ~/.config/xfce4-theme-switcher/uninstall.sh
	[ -f $WORKING_DIR/gtkrc-2.0 ] && cp -rf $WORKING_DIR/gtkrc-2.0 ~/.gtkrc-2.0

	. $WORKING_DIR/theme.ini
	xfconf-query -c xsettings -p /Net/ThemeName -n -t string -s "$GTK_THEME"
	xfconf-query -c xfwm4 -p /general/theme -n -t string -s "$WM_THEME"
	xfconf-query -c xfwm4 -p /general/button_layout -n -t string -s "$WM_BUTTON_LAYOUT"
	[ -n "$WM_TITLE_FONT" ] && xfconf-query -c xfwm4 -p /general/title_font -n -t string -s "$WM_TITLE_FONT"
	xfconf-query -c xsettings -p /Net/IconThemeName -n -t string -s "$ICON_THEME"
	xfconf-query -c xsettings -p /Gtk/CursorThemeName -n -t string -s "$CURSOR_THEME"
	xfconf-query -c xsettings -p /Gtk/FontName -n -t string -s "$FONT"
	[ -n "$MONOSPACE_FONT" ] && xfconf-query -c xsettings -p /Gtk/MonospaceFontName -n -t string -s "$MONOSPACE_FONT"
	[ -n "$DECORATION_LAYOUT" ] && xfconf-query -c xsettings -p /Gtk/DecorationLayout -n -t string -s "$DECORATION_LAYOUT"
	xfconf-query -c xfwm4 -p /general/workspace_count -s "$WORKSPACE_COUNT"

	$XFCE4_PANEL_PROFILES load $PANEL_PROFILE
	killall plank
	rm -rf ~/.config/plank
	[ -d $WORKING_DIR/config ] && cp -rf $WORKING_DIR/config/* ~/.config/
	[ -d $WORKING_DIR/local ] && cp -rf $WORKING_DIR/local/* ~/.local/

	if [[ -f ~/.config/autostart/Conky-session.desktop ]]; then
		sed -i "s|HOMEDIR|$HOME|g" ~/.config/autostart/Conky-session.desktop
	fi

	if [[ -d $WORKING_DIR/plank-items ]]; then
		mkdir -p ~/.plank-items
		cp -rf $WORKING_DIR/plank-items/* ~/.plank-items/
	fi

	if [ -f $WORKING_DIR/plank.ini ]; then
		dconf reset -f /net/launchpad/plank/docks/
		cat $WORKING_DIR/plank.ini | dconf load /net/launchpad/plank/docks/
	fi

	if [[ -d $WORKING_DIR/conky ]]; then
		killall conky
		mkdir -p ~/.config/conky/
		cp -rf $WORKING_DIR/conky/* ~/.config/conky/
		sed -i "s|HOMEDIR|$HOME|g" ~/.config/conky/conky-sessionfile
		if [[ -d ~/.config/conky/fonts ]]; then
			mkdir -p ~/.fonts/
			cp -rf ~/.config/conky/fonts/* ~/.fonts/
		fi
	fi
	nohup xfce4-panel > /dev/null 2>&1 &
	if [ -f $WORKING_DIR/postinstall.sh ]; then
		bash $WORKING_DIR/postinstall.sh
		cp -rf $WORKING_DIR/postinstall.sh ~/.config/xfce4-theme-switcher/postinstall.sh
	fi
	rm -rf $WORKING_DIR
}

backup()
{
	cat << EOF > $WORKING_DIR/theme.ini
GTK_THEME="$(xfconf-query -c xsettings -p /Net/ThemeName 2> /dev/null)"
WM_THEME="$(xfconf-query -c xfwm4 -p /general/theme 2> /dev/null)"
WM_TITLE_FONT="$(xfconf-query -c xfwm4 -p /general/title_font 2> /dev/null)"
WM_BUTTON_LAYOUT="$(xfconf-query -c xfwm4 -p /general/button_layout 2> /dev/null)"
ICON_THEME="$(xfconf-query -c xsettings -p /Net/IconThemeName 2> /dev/null)"
CURSOR_THEME="$(xfconf-query -c xsettings -p /Gtk/CursorThemeName 2> /dev/null)"
FONT="$(xfconf-query -c xsettings -p /Gtk/FontName 2> /dev/null)"
MONOSPACE_FONT="$(xfconf-query -c xsettings -p /Gtk/MonospaceFontName 2> /dev/null)"
DECORATION_LAYOUT="$(xfconf-query -c xsettings -p /Gtk/DecorationLayout 2> /dev/null)"
WORKSPACE_COUNT="$(xfconf-query -c xfwm4 -p /general/workspace_count 2> /dev/null)"
EOF
	$XFCE4_PANEL_PROFILES save $PANEL_PROFILE

	if [ -f ~/.config/xfce4-theme-switcher/postinstall.sh ]; then
		mkdir -p $WORKING_DIR/config/xfce4-theme-switcher
		cp -rf ~/.config//xfce4-theme-switcher/postinstall.sh $WORKING_DIR/postinstall.sh
	else
		cat << EOF > $WORKING_DIR/postinstall.sh
#!/bin/bash
# Postinstall will run after installation of this theme

EOF
	fi
	chmod a+x $WORKING_DIR/postinstall.sh

	if [ -f ~/.config/xfce4-theme-switcher/uninstall.sh ]; then
		mkdir -p $WORKING_DIR/config/xfce4-theme-switcher
		cp -rf ~/.config//xfce4-theme-switcher/uninstall.sh $WORKING_DIR/uninstall.sh
	else
		cat << EOF > $WORKING_DIR/uninstall.sh
#!/bin/bash
# Uninstall is for clean up settings of this theme
# before change to the new theme

EOF
	fi
	chmod a+x $WORKING_DIR/uninstall.sh

	echo "0.2" > $WORKING_DIR/version

	if [ -f ~/.config/gtk-3.0/* ]; then
		mkdir -p $WORKING_DIR/config/gtk-3.0
		cp -rf ~/.config/gtk-3.0/* $WORKING_DIR/config/gtk-3.0/
	fi
	[[ -f ~/.gtkrc-2.0 ]] && cp -rf ~/.gtkrc-2.0 $WORKING_DIR/gtkrc-2.0
	[[ -f ~/.gtkrc-3.0 ]] && cp -rf ~/.gtkrc-3.0 $WORKING_DIR/gtkrc-3.0

	mkdir -p $WORKING_DIR/config/xfce4/panel/
	cp ~/.config/xfce4/panel/whiskermenu-* $WORKING_DIR/config/xfce4/panel/ 2>/dev/null

	if [[ -d ~/.config/autostart ]]; then
		mkdir -p $WORKING_DIR/config/autostart/
		cp -rf ~/.config/autostart/* $WORKING_DIR/config/autostart/
	fi
	
	if [[ -d ~/.config/plank ]]; then
		mkdir -p $WORKING_DIR/config/
		cp -rf ~/.config/plank $WORKING_DIR/config/
		dconf dump /net/launchpad/plank/docks/ > ${WORKING_DIR}plank.ini
		if [[ -f ~/.config/autostart/Plank.desktop ]]; then
			if ! grep -q "^nohup plank" $WORKING_DIR/postinstall.sh ; then
				echo -e "\nnohup plank > /dev/null 2>&1&" >> $WORKING_DIR/postinstall.sh
			fi
		fi

		if [[ -d ~/.plank-items ]]; then
			mkdir -p $WORKING_DIR/plank-items/
			cp -rf ~/.plank-items/* $WORKING_DIR/plank-items/
		fi
	fi

	if [[ -d ~/.config/conky ]]; then
		mkdir -p $WORKING_DIR/conky/
		cp -rf ~/.config/conky/* $WORKING_DIR/conky/
		sed -i "s|$HOME|HOMEDIR|g" $WORKING_DIR/conky/conky-sessionfile
		if [[ -f ~/.config/autostart/Conky-session.desktop ]]; then
			mkdir -p $WORKING_DIR/config/autostart/
			cp ~/.config/autostart/Conky-session.desktop $WORKING_DIR/config/autostart/
			sed -i "s|$HOME|HOMEDIR|g" $WORKING_DIR/config/autostart/Conky-session.desktop
			if ! grep -q "^nohup sh ~/.config/conky/conky-sessionfile" $WORKING_DIR/postinstall.sh; then
				echo -e "\nnohup sh ~/.config/conky/conky-sessionfile > /dev/null 2>&1&" >> $WORKING_DIR/postinstall.sh
			fi
		fi
	fi
	
	if [[ -d ~/.config/autostart ]]; then
		mkdir -p $WORKING_DIR/config/autostart/
		cp -rf ~/.config/autostart/* $WORKING_DIR/config/autostart/
	fi
	
	if [[ -d ~/.config/gtk-3.0 ]]; then
		mkdir -p $WORKING_DIR/config/gtk-3.0/
		cp -rf ~/.config/gtk-3.0/* $WORKING_DIR/config/gtk-3.0/
	fi
	
	if [[ -d ~/.config/xfce4/terminal ]]; then
		mkdir -p $WORKING_DIR/config/xfce4/terminal
		cp -rf ~/.config/xfce4/terminal/terminalrc $WORKING_DIR/config/xfce4/terminal/
	fi
	
	if [[ -d ~/.config/menu ]]; then
		mkdir -p $WORKING_DIR/config/menu/
		cp -rf ~/.config/menu/* $WORKING_DIR/config/menu/
	fi
	
	if [[ -d ~/.config/rofi ]]; then
		mkdir -p $WORKING_DIR/config/rofi/
		cp -rf ~/.config/rofi/* $WORKING_DIR/config/rofi/
	fi
	
	if [[ -d ~/.config/ulauncher ]]; then
		mkdir -p $WORKING_DIR/config/ulauncher/
		cp -rf ~/.config/ulauncher/* $WORKING_DIR/config/ulauncher/
	fi

	if [[ -d ~/.local/share/applications ]]; then
		mkdir -p $WORKING_DIR/local/share/applications
		cp -rf ~/.local/share/applications/* $WORKING_DIR/local/share/applications
	fi

	if [[ -d ~/.local/share/fonts ]]; then
		mkdir -p $WORKING_DIR/local/share/fonts
		cp -rf ~/.local/share/fonts/* $WORKING_DIR/local/share/fonts
	fi
	
	if [[ -d ~/.local/share/icons ]]; then
		mkdir -p $WORKING_DIR/local/share/icons
		cp -rf ~/.local/share/icons/* $WORKING_DIR/local/share/icons
	fi
	
	if [[ -d ~/.local/share/plank ]]; then
		mkdir -p $WORKING_DIR/local/share/plank
		cp -rf ~/.local/share/plank/* $WORKING_DIR/local/share/plank
	fi

	if [[ -d ~/.local/share/rofi ]]; then
		mkdir -p $WORKING_DIR/local/share/rofi/themes
		cp -rf ~/.local/share/rofi/* $WORKING_DIR/local/share/rofi
	fi
	
	BACKUP_FILE=${BACKUP_DIR}theme-backup-$(date +%Y.%m.%d-%H.%M.%S).tar.gz
	tar -czf $BACKUP_FILE -C $WORKING_DIR/ .
	rm -rf $WORKING_DIR
}


restore()
{
	if [ -r $BACKUP_DIR$1 ]; then
		tar -xzf $BACKUP_DIR$1 -C $WORKING_DIR/
		set_settings
	else
		echo "$1 does not exist"
	fi
}

set_theme()
{
	if [ -r $SHARE_DIR/themes/$1 ]; then
		tar -xzf $SHARE_DIR/themes/$1 -C $WORKING_DIR/
		set_settings
	else
		echo "$1 does not exist"
	fi
}

load_theme()
{
	if [ -r $1 ]; then
		tar -xzf $1 -C $WORKING_DIR/
		set_settings
	else
		echo "$1 does not exist"
	fi
}

unpack_theme()
{
	rm -rf $BACKUP_DIR/*
	tar -xzf $THEME_NAME -C $BACKUP_DIR .
	mkdir $BACKUP_DIR/panel-profile
	tar xzf $BACKUP_DIR/panel-profile.tar.gz -C $BACKUP_DIR/panel-profile/ .
	rm -rf $BACKUP_DIR/panel-profile.tar.gz
}

create_skeleton()
{
	cd $BACKUP_DIR
	tar xzf $BACKUP_FILE -C $BACKUP_DIR .
	rm -rf $BACKUP_FILE
	mkdir panel-profile
	tar xzf panel-profile.tar.gz -C panel-profile/ .
	rm -rf panel-profile.tar.gz
}

pack_skeleton()
{
	cd $BACKUP_DIR
	tar czf panel-profile.tar.gz -C panel-profile/ .
	rm -rf panel-profile/
	cd ..
	tar czf $SKEL_NAME.tar.gz -C $BACKUP_DIR .
	cd $BACKUP_DIR
	mkdir panel-profile
	tar xzf panel-profile.tar.gz -C panel-profile/ .
	rm -rf panel-profile.tar.gz
}

case $1 in
	--backup | -b)
		backup
		echo -e 'Backup was created like\n ' $BACKUP_FILE
		notify-send -i dialog-information 'Desktop settings backuped'
	;;
	--restore | -r)
		if [[ $# -gt 1 ]]; then
			restore $2
			sleep 1
			notify-send -i dialog-information 'Desktop settings restored'
		else
			help
		fi
	;;
	--restore-last | -rl)
		last_backup=$(ls -1 $BACKUP_DIR | tail -1)
		if [ -z $last_backup ]; then
			echo "No backup file found"
		else
			restore $last_backup
			sleep 1
			notify-send -i dialog-information 'Desktop settings restored'
		fi
	;;
	--theme-set | -t)
		if [[ $# -ne 1 ]]; then
			set_theme $2
		else
			help
		fi
	;;
	--theme-load |-tl)
		if [ $# -ne 1 ]; then
			load_theme $2
		else
			help
		fi
	;;
	--theme-unpack |-tu)
		if [ $# -ne 2 ]; then
			BACKUP_DIR=$2
			THEME_NAME=$3
			unpack_theme
		else
			help
		fi
	;;
	--list | -l)
		echo -e "------------\nThemes list:\n------------"
		ls -1 $SHARE_DIR/themes
		echo -e "------------\n"
		echo -e "------------\nBackup list:\n------------"
		ls -1 $BACKUP_DIR
		echo "------------"
	;;
	--list-backups | -lb)
		echo -e "------------\nBackup list:\n------------"
		ls -1 $BACKUP_DIR
		echo "------------"
	;;
	--list-themes | -lt)
		echo -e "------------\nThemes list:\n------------"
		ls -1 $SHARE_DIR/themes
		echo "------------"
	;;
	--skeleton-create | -sc)
		if [ $# -gt 1 ]; then
			BACKUP_DIR=$2
			if [[ -d $BACKUP_DIR ]]; then
				rm -rf $BACKUP_DIR/*
				backup
				create_skeleton
				echo 'Skeleton  created in ' $BACKUP_DIR
				sleep 1
				notify-send -i dialog-information 'Skeleton created'
			else
				echo "Directory $WORKING_DIR do not exists !!!"
			fi
		else
			help
		fi
	;;
	--skeleton-pack | -sp)
		if [ $# -gt 2 ]; then
			BACKUP_DIR=$2
			if [ -d $BACKUP_DIR ]; then
				SKEL_NAME=$3
				pack_skeleton
				echo "Skeleton packed like $SKEL_NAME.tar.gz"
				sleep 1
				notify-send -i dialog-information 'Skeleton packed'
			else
				echo "Wrong arguments !!!"
			fi
		else
			help
		fi
	;;
	*)
		help
	;;
esac
