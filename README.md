# Xfce4 Theme Switcher

Xfce4 Theme Switcher is collection of the configuration utilities for a fast
switching between preconfigured Xfce4 themes.

The theme can containing autostart files (~/.config/autostart),  conky (~/.config/conky), plank dock (~/.config/plank), rofi (~/.config/rofi & ~/.local/share/rofi), fonts (~/.local/share/fonts), icons (~/.local/share/icons), background, Xfce4 panels and valid Xfce4 theme.

Due the different licenses of the themes items is this software distributed without the themes. The themes you can find [here](https://gitlab.com/linux-stuffs/xts-themes).

Xfce4 theme switcher can be used in two modes:

* **GUI mode**

* **Terminal mode**

## GUI mode

When you run command xfce4-theme-switcher without the parameters, the Xfce4 Theme
Switcher will start in the GUI mode.

![Xfce4 Theme Switcher ](img-git/xfce4-theme-switcher.png)

## Terminal mode
When you run command xfce4-theme-switcher with some of these parameters, the
Xfce4 Theme Switcher will start in the terminal mode.

Usage: `xfce4-theme-switcher [OPTION]`

* `h,  --help`  
show help

* `-b,  --backup`  
backup actual settings of the Xfce4 theme

* `-r,  --restore file file-name`  
restore settings from file (from the ~/.backup-xts)

* `-rl, --restore-last`  
restore last backup file (from the ~/.backup-xts)

* `-l,  --list`  
list of all themes and backup files

* `-lb, --list-backups`  
list of backup files only (from the ~/.backup-xts)

* `-lt, --list-themes`  
lits of themes only (/usr/share/xconf-theme-switcher/themes)

* `-sc, --skeleton-create /full-path-directory/`  
create skeleton from the actual settings in the full-path directory

* `-sp, --skeleton-pack full-path/ theme-name`  
pack the skeleton from the full-path directory like theme-name (the file extension tar.gz is automatically added)

* `-t,  --theme-set theme-name`  
set theme from the themes directory (/usr/share/xconf-theme-switcher/themes)

* `-tl, --theme-load /full-path-directory/file-name`  
load theme from selected file


## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xfce4-theme-switcher/):
```
git clone https://aur.archlinux.org/xfce4-theme-switcher.git
cd xfce4-theme-switcher/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest xfce4-theme-switcher package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./xfce4-theme-switcher*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xfce4-theme-switcher*.pkg.tar.xz
```

### Build own Debian package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xfce4-theme-switcher*.deb
```

## TROUBLESHOOTING

### GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown

When you obtained error message: _GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name org.freedesktop.Notifications was not provided by any .service files_, then you probably missing file _org.freedesktop.Notifications.service_ into _/usr/share/dbus-1/services/_ directory.

For solution create (like root) file _/usr/share/dbus-1/services/org.freedesktop.Notifications.service_:
```
[D-BUS Service]
Name=org.freedesktop.Notifications.service
Exec=/usr/lib/xfce4/notifyd/xfce4-notifyd
```

After reboot the trouble shall be solved (tested on the Arch Linux).